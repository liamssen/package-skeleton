## Package skeleton

This repository contains a skeleton to easily set-up a new (Composer) package.

Replace these variables in all files of the repository:
 - `:author_name` (example: 'Liam Boer')
 - `:author_username` (example: 'imbue')
 - `:author_email` (example: 'liam@tool.com')
 - `:package_name` (example: 'php-tool')
 - `:package_description` (example: 'A tool')
 - `:vendor` (example: 'imbue')
 - `:namespace_vendor` (example: 'Imbue')
 - `:namespace_tool_name` (example: 'Tool')
 
**When you are done with the steps above delete everything above!**

# :package_description

[![Latest Version on Packagist](https://img.shields.io/packagist/v/:vendor/:package_name.svg?style=flat-square)](https://packagist.org/packages/:vendor/:package_name)
[![Total Downloads](https://img.shields.io/packagist/dt/:vendor/:package_name.svg?style=flat-square)](https://packagist.org/packages/:vendor/:package_name)


> This is where your description should go. Try and limit it to a paragraph or two.

> Add a screenshot of the tool here.

## Installation

You can install the package via composer:

```bash
composer require :vendor/:package_name
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Security

If you discover any security related issues, please email :author_email or use the issue tracker.

## Credits

- [:author_name](https://github.com/:author_username)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.